source("run_DESeq.R")
source("run_edgeR.R")
source("run_PoisSeq.R")
source("run_limma.R")
source("run_baySeq.R")
source("run_NOISeq.R")

DEanalysis <- function(cond.name){
  ## Run a set of DE analysis using differnt R packages
  ##
  ## cond.name = a list of conditions and replicates to comapre
  ## For example, cond.name <- list(GM12892=c(2,3), H1-hESC = c(1,2) )
  ## will compare [H1-hESC_Rep1,H1-hESC_Rep2] to [GM12892_Rep2_V2, GM12892_Rep3_V2]
  ## Or, cond.name <- list('GM12892' = c(1,2,3), 'H1-hESC' = c(1,2,3,4))
  ## or  cond.name <- list('H1-hESC_12'=c(1,2), 'H1-hESC_34'=c(3,4)) will compare
  ## [H1-hESC_1, H1-hESC_2] to [H1-hESC_3, H1-hESC_4]
  ## compares all GM12892 to H1-hESC
  ## must run this script in the project 'src' directory
  if(length(cond.name) != 2){
    stop("cond.name must include two cell types")
  }
  ## load BR.counts
  load("../data/BiolRep/BR_HTSeq.Rdata")

  ## set conditions
  samples <- lapply(names(cond.name), function(x) paste(x, '_Rep',cond.name[[x]],sep=''))
  samples <- unlist(samples)

  ## subselect samples
  counts.dat <- BR.counts[,gsub("_[0-9]+", '', samples)]
  conditions <- c(rep("condA", length(cond.name[[1]])), rep("condB", length(cond.name[[2]])))

  results.full <- list()

  ## ###############
  ## DESeq analysis
  ## ###############
  results.full["DESeq"] <- list(runDESeq(counts.dat, conditions))

  ## ###############
  ## edgeR analysis
  ## ###############
  results.full["edgeR"] <- list(runedgeR(counts.dat, conditions))

  ## ################
  ## PoissonSeq
  ## ################
  count.cut=0
  results.full["PoissonSeq"] <- list(runPoissonSeq(counts.dat, conditions, count.cut))

  ## ###############
  ## limma + quantile norm
  ## ##############
  if(length(conditions) > 2){
    useVoom=FALSE
    results.full["limmaQN"] <- list(runLimmaQuantile(counts.dat, conditions, useVoom))
  }

  ## ################
  ## limma + voom
  ## ################
  if(length(conditions) > 2){
    useVoom=TRUE
    results.full["limmaVoom"] <- list(runLimmaQuantile(counts.dat, conditions, useVoom))
  }


  ## ############
  ## baySeq
  ## ############
  results.full["baySeq"] <- list(runBaySeq(counts.dat, conditions))
  ## save

  ## #############
  ## NOISeq
  ## ############
##  results.full["NOISeq"] <- list(runNOISeq(counts.dat, conditions))
  
  save(results.full, file=paste("../data/BiolRep/Results_", paste(names(cond.name),collapse='_vs_'),
                       "_",Sys.Date(),".Rdata", sep=''))

}
