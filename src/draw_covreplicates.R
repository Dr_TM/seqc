superpaste <- function(pastelist){
	result = ''
	for(i in 1:length(pastelist))
	{
		result = paste(result,pastelist[i],sep="")
	}
	return(result)
}

barplot.error <- function(signal,sd.signal,col=heat.colors(dim(signal)[1]))
{
	positions = barplot(signal,col=col,density=seq(10, 100, 30),beside=T,ylim=c(min(signal-sd.signal),max(sd.signal+signal)))
	for(i in 1:dim(signal)[1])
	{
		for(j in 1:dim(signal)[2])
		{
			arrows(positions[i,j],sd.signal[i,j]+signal[i,j], positions[i,j], signal[i,j]-sd.signal[i,j], angle=90, code=3, length=0.05)
		}
	}
	
}

##
## collect DE results by method
##
selectdegenes <- function(table, p.thresh, lfc.thresh, method)
{
	if(method == "edger")
	{
		table = res$table
		names = row.names(table)
		p.index = 3
		lfc.index = 1
	}
	if(method == "deseq")
	{
		table = table[-which(is.na(table[,8])),]
		names = table[,1]
		p.index = 8
		lfc.index = 6		
	}
	if(method == "limmaqn")
	{
		names = table[,1]
		p.index = 6
		lfc.index = 2		
	}
	if(method == "voom")
	{
		names = table[,1]
		p.index = 6
		lfc.index = 2	
	}
	if(method == "poisson")
	{
		names = row.names(table)
		p.index = 4
		lfc.index = 6		
	}
	return(names[intersect(which(table[,p.index]<p.thresh),which(abs(table[,lfc.index])>lfc.thresh))])

}

## Generate the set of predictions that
## will define the true negatives. All genes
## NOT included in this set are true negative
## and therefore, DE prediction outside
## this set is FP.
FP_ref <- function(all.names, method){
  switch(method,
         deseq=x.FP.ref(all.names[-which(names(all.names) =="DESeq")]),
         edger=x.FP.ref(all.names[-which(names(all.names) == "edgeR")]),
         limmaqn=x.FP.ref(all.names),
         voom=x.FP.ref(all.names[-which(names(all.names) == "limmaVoom")]),
         poisson=x.FP.ref(all.names))
}

## Auxiliary recursive function to
## find union of list of genes
x.FP.ref <- function(gene_names){
  if(length(gene_names)==2) {
    return (union(gene_names[[1]], gene_names[[2]]))
  }else{
    return(union(gene_names[[1]], x.FP.ref(gene_names[-1])))
  }
}

## Find intersection of list of genes
Ref_set <- function(gene_names){
  if(length(gene_names)==2) {
    return (intersect(gene_names[[1]], gene_names[[2]]))
  }else{
    return(intersect(gene_names[[1]], Ref_set(gene_names[-1])))
  }
}

## p-value threshold
p.thresh = 0.05
## log fold-change threshold
lfc.thresh = 1

## getting expression quantiles
load("../data/HTSeq.RData")
mode(HTSeq.dat) = "numeric"
signal = rowMeans(HTSeq.dat)
quantiles = quantile(signal)
load("../data/trueset.Rdata")

#####################
## True Positive set
#####################
## total.res <- all.names$intersection.genes ## 7-way intersection

## alternative definitions of 'total.set'
total.res <- Ref_set(all.names[c("DESeq", "edgeR", "limmaVoom", "baySeq")])

total.res.1q = intersect(total.res,row.names(HTSeq.dat)[which(signal<=quantiles[2])])
total.res.2q = intersect(total.res,row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),which(signal<=quantiles[3]))])
total.res.3q = intersect(total.res,row.names(HTSeq.dat)[intersect(which(signal>quantiles[3]),which(signal<=quantiles[4]))])
total.res.4q = intersect(total.res,row.names(HTSeq.dat)[which(signal>quantiles[4])])

## Base directory where DE results are stored
kDataBaseDir="/scratchBulk/dob2014/SEQC/Coverage_Depth/12.17.12"

methods = c("deseq","edger","limmaqn","voom","poisson")
col.methods <- c("#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99")
cov = c("05","10","20","30","40","50")
dim_names <- list(replicates=c("2rep", "3rep", "4rep", "5rep"),
                 coverage=c("5%", "10%", "20%", "30%", "40%", "50%", "100%"))

## Iterate over methods
for(index.method in 1:length(methods))
{
	method = methods[index.method]
	col = col.methods[index.method]
	#total conserved and added genes
	sensitivity = matrix(1,nrow=4,ncol=7, dimnames=dim_names)
	sd.sensitivity = matrix(0,nrow=4,ncol=7, dimnames=dim_names)
	fp = matrix(0,nrow=4,ncol=7, dimnames=dim_names)
	sd.fp = matrix(0,nrow=4,ncol=7, dimnames=dim_names)

	#same thing for each quantile
	sensitivity.1q = matrix(1,nrow=4,ncol=7, dimnames=dim_names)
	sd.sensitivity.1q = matrix(0,nrow=4,ncol=7, dimnames=dim_names)
	fp.1q = matrix(0,nrow=4,ncol=7, dimnames=dim_names)
	sd.fp.1q = matrix(0,nrow=4,ncol=7, dimnames=dim_names)
	sensitivity.2q = matrix(1,nrow=4,ncol=7, dimnames=dim_names)
	sd.sensitivity.2q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	fp.2q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	sd.fp.2q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	sensitivity.3q = matrix(1,nrow=4,ncol=7,dimnames=dim_names)
	sd.sensitivity.3q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	fp.3q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	sd.fp.3q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	sensitivity.4q = matrix(1,nrow=4,ncol=7,dimnames=dim_names)
	sd.sensitivity.4q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	fp.4q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)
	sd.fp.4q = matrix(0,nrow=4,ncol=7,dimnames=dim_names)

        #####################
        ## True Negative Set
        ## all the genes that
        ## were NOT predicted by any of the other methods
        ## FP = length(setdiff( <method X DE>, union.res))
        ######################
        union.res <- FP_ref(all.names, methods[index.method])
          
        union.res.1q <- intersect(union.res,row.names(HTSeq.dat)[which(signal<=quantiles[2])])
        union.res.2q <- intersect(union.res,row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),which(signal<=quantiles[3]))])
        union.res.3q <- intersect(union.res,row.names(HTSeq.dat)[intersect(which(signal>quantiles[3]),which(signal<=quantiles[4]))])
        union.res.4q <- intersect(union.res,row.names(HTSeq.dat)[which(signal>quantiles[4])])

	## Iterate over sampling size/coverage
	for(i in 1:length(cov)) ## coverage index
	{
		for(j in 2:4) ## replicate index from 2 to 4 replicates
		{
			total.conserved = list()
			total.added = list()
			total.conserved.1q = list()
			total.added.1q = list()
			total.conserved.2q = list()
			total.added.2q = list()
			total.conserved.3q = list()
			total.added.3q = list()
			total.conserved.4q = list()
			total.added.4q = list()
			combinations = combn(5,j) ## all j combinations from 5
			couples = combn(dim(combinations)[2],2) ## all pairwise combinations
			for(k in 1:dim(couples)[2]) 
			{
				for(l in 1:5)
				{
                                  ## files are named as follows:
                                  ## basedir/[method]_[coverage]_[number of replicates]_[group.combination]_[random subsampling round]
                                  ## last index are the 5 random sampling for each coverage:replicate:group
					file = superpaste(c(kDataBaseDir,"/",method,"/",method,"_",cov[i],"_",
                                          as.character(j),"_",as.character(k),"_",as.character(l),".Rdata"))
					load(file)
					cat(file,"\n")

                                        ## update the total conserved from total.res=identified by all methods
					total.conserved = c(total.conserved,
                                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res)))

                                        ## update the specificity. Newly added genes that were not found in union.res
                                        ## these are the genes that were identified but are not present in the intersection with total.res
                                        total.added = c(total.added,
                                          length(setdiff(selectdegenes(res,p.thresh,lfc.thresh,method),union.res)))			

                                        total.conserved.1q = c(total.conserved.1q,
                                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.1q)))
					total.added.1q = c(total.added.1q,
                                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                           row.names(HTSeq.dat)[which(signal<=quantiles[2])]), union.res.1q)))

                                        total.conserved.2q = c(total.conserved.2q,
                                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.2q)))
					total.added.2q = c(total.added.2q,
                                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                           row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),
                                                                                          which(signal<=quantiles[3]))]), union.res.2q)))

                                        total.conserved.3q = c(total.conserved.3q,
                                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.3q)))
					total.added.3q = c(total.added.3q,
                                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                           row.names(HTSeq.dat)[intersect(which(signal<=quantiles[4]),
                                                                                          which(signal>quantiles[3]))]), union.res.3q)))
                                        
					total.conserved.4q = c(total.conserved.4q,
                                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.4q)))
					total.added.4q = c(total.added.4q,
                                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                           row.names(HTSeq.dat)[which(signal>quantiles[4])]), union.res.4q)))		
				}
			}

                        ## Summarize mean and sd
			sensitivity[j-1,i] = mean(unlist(total.conserved))/length(total.res)
			sd.sensitivity[j-1,i] = sd(unlist(total.conserved)/length(total.res))
			fp[j-1,i] = mean(unlist(total.added))
                        sd.fp[j-1,i] = sd(unlist(total.added))
			sensitivity.1q[j-1,i] = mean(unlist(total.conserved.1q))/length(total.res.1q)
			sd.sensitivity.1q[j-1,i] = sd(unlist(total.conserved.1q)/length(total.res.1q))
			fp.1q[j-1,i] = mean(unlist(total.added.1q))
			sd.fp.1q[j-1,i] = sd(unlist(total.added.1q)) 
			sensitivity.2q[j-1,i] = mean(unlist(total.conserved.2q))/length(total.res.2q)
			sd.sensitivity.2q[j-1,i] = sd(unlist(total.conserved.2q)/length(total.res.2q))
			fp.2q[j-1,i] = mean(unlist(total.added.2q))
			sd.fp.2q[j-1,i] = sd(unlist(total.added.2q)) 
			sensitivity.3q[j-1,i] = mean(unlist(total.conserved.3q))/length(total.res.3q)
			sd.sensitivity.3q[j-1,i] = sd(unlist(total.conserved.3q)/length(total.res.3q))
			fp.3q[j-1,i] = mean(unlist(total.added.3q))
			sd.fp.3q[j-1,i] = sd(unlist(total.added.3q))
			sensitivity.4q[j-1,i] = mean(unlist(total.conserved.4q))/length(total.res.4q)
			sd.sensitivity.4q[j-1,i] = sd(unlist(total.conserved.4q)/length(total.res.4q))
			fp.4q[j-1,i] = mean(unlist(total.added.4q))
			sd.fp.4q[j-1,i] = sd(unlist(total.added.4q))

		}	

                ## 5 replicates data
		total.conserved = list()
		total.added = list()
		total.conserved.1q = list()
		total.added.1q = list()
		total.conserved.2q = list()
		total.added.2q = list()
		total.conserved.3q = list()
		total.added.3q = list()
		total.conserved.4q = list()
		total.added.4q = list()
		for(j in 1:5) ## index over the 5 random subsampling
		{
			file = superpaste(c(kDataBaseDir,"/",method,"/",method,"_",cov[i],"_5_1_",as.character(j),".Rdata"))
			load(file)
			total.conserved = c(total.conserved,
                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res)))
			total.added = c(total.added,
                          length(setdiff(selectdegenes(res,p.thresh,lfc.thresh,method), union.res)))			

                        ## Q1 bottom 25% 
                        total.conserved.1q = c(total.conserved.1q,
                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.1q)))
			total.added.1q = c(total.added.1q,
                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                           row.names(HTSeq.dat)[which(signal<=quantiles[2])]), union.res.1q)))
                        ## Q2 50% quantile
                        total.conserved.2q = c(total.conserved.2q,
                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.2q)))
			total.added.2q = c(total.added.2q,
                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                           row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),which(signal<=quantiles[3]))]),union.res.2q)))

                        ## Q3 3rd highest expression quartile
			total.conserved.3q = c(total.conserved.3q,
                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.3q)))
			total.added.3q = c(total.added.3q,
                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                           row.names(HTSeq.dat)[intersect(which(signal<=quantiles[4]),which(signal>quantiles[3]))]), union.res.3q)))

                        ## Top 25% expression quantile
			total.conserved.4q = c(total.conserved.4q,
                          length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.4q)))
			total.added.4q = c(total.added.4q,
                          length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                           row.names(HTSeq.dat)[which(signal>quantiles[4])]), union.res.4q)))		

		}
                
		sensitivity[4,i] = mean(unlist(total.conserved))/length(total.res)
		sd.sensitivity[4,i] = sd(unlist(total.conserved)/length(total.res))
		fp[4,i] = mean(unlist(total.added))
		sd.fp[4,i] = sd(unlist(total.added))
		sensitivity.1q[4,i] = mean(unlist(total.conserved.1q))/length(total.res.1q)
		sd.sensitivity.1q[4,i] = sd(unlist(total.conserved.1q)/length(total.res.1q))
		fp.1q[4,i] = mean(unlist(total.added.1q))
		sd.fp.1q[4,i] = sd(unlist(total.added.1q))
		sensitivity.2q[j-1,i] = mean(unlist(total.conserved.2q))/length(total.res.2q)
		sd.sensitivity.2q[4,i] = sd(unlist(total.conserved.2q)/length(total.res.2q))
		fp.2q[4,i] = mean(unlist(total.added.2q))
		sd.fp.2q[4,i] = sd(unlist(total.added.2q))
		sensitivity.3q[j-1,i] = mean(unlist(total.conserved.3q))/length(total.res.3q)
		sd.sensitivity.3q[4,i] = sd(unlist(total.conserved.3q)/length(total.res.3q))
		fp.3q[4,i] = mean(unlist(total.added.3q))
		sd.fp.3q[4,i] = sd(unlist(total.added.3q))
		sensitivity.4q[4,i] = mean(unlist(total.conserved.4q))/length(total.res.4q)
		sd.sensitivity.4q[4,i] = sd(unlist(total.conserved.4q)/length(total.res.4q))
		fp.4q[4,i] = mean(unlist(total.added.4q))
		sd.fp.4q[4,i] = sd(unlist(total.added.4q))
	
	}

        ## Full coverage, 100% if the reads
        ## 2:4 replicates
	for(j in 2:4)
	{
		total.conserved = list()
		total.added = list()
		total.conserved.1q = list()
		total.added.1q = list()
		total.conserved.2q = list()
		total.added.2q = list()
		total.conserved.3q = list()
		total.added.3q = list()
		total.conserved.4q = list()
		total.added.4q = list()
		combinations = combn(5,j)
		couples = combn(dim(combinations)[2],2)
		for(k in 1:dim(couples)[2])
		{
			file = superpaste(c(kDataBaseDir,"/",method,"/",method,"_100_",as.character(j),"_",as.character(k),"_1.Rdata"))
				load(file)
				cat(file,"\n")
				total.conserved = c(total.conserved,
                                  length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res)))
				total.added = c(total.added,
                                  length(setdiff(selectdegenes(res,p.thresh,lfc.thresh,method),union.res)))
			
				total.conserved.1q = c(total.conserved.1q,
                                  length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.1q)))
				total.added.1q = c(total.added.1q,
                                  length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                   row.names(HTSeq.dat)[which(signal<=quantiles[2])]), union.res.1q)))
                        
				total.conserved.2q = c(total.conserved.2q,
                                  length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.2q)))
				total.added.2q = c(total.added.2q,
                                  length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                   row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),
                                                                                  which(signal<=quantiles[3]))]), union.res.2q)))
                        
				total.conserved.3q = c(total.conserved.3q,
                                  length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.3q)))
				total.added.3q = c(total.added.3q,
                                  length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                   row.names(HTSeq.dat)[intersect(which(signal<=quantiles[4]),
                                                                                  which(signal>quantiles[3]))]), union.res.3q)))
                        
				total.conserved.4q = c(total.conserved.4q,
                                  length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.4q)))
				total.added.4q = c(total.added.4q,
                                  length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
                                                   row.names(HTSeq.dat)[which(signal>quantiles[4])]), union.res.4q)))		
		}
		sensitivity[j-1,7] = mean(unlist(total.conserved))/length(total.res)
		sd.sensitivity[j-1,7] = sd(unlist(total.conserved)/length(total.res))
		fp[j-1,7] = mean(unlist(total.added))
		sd.fp[j-1,7] = sd(unlist(total.added))
		sensitivity.1q[j-1,7] = mean(unlist(total.conserved.1q))/length(total.res.1q)
		sd.sensitivity.1q[j-1,7] = sd(unlist(total.conserved.1q)/length(total.res.1q))
		fp.1q[j-1,7] = mean(unlist(total.added.1q))
		sd.fp.1q[j-1,7] = sd(unlist(total.added.1q))
		sensitivity.2q[j-1,7] = mean(unlist(total.conserved.2q))/length(total.res.2q)
		sd.sensitivity.2q[j-1,7] = sd(unlist(total.conserved.2q)/length(total.res.2q))
		fp.2q[j-1,7] = mean(unlist(total.added.2q))
		sd.fp.2q[j-1,7] = sd(unlist(total.added.2q))
		sensitivity.3q[j-1,7] = mean(unlist(total.conserved.3q))/length(total.res.3q)
		sd.sensitivity.3q[j-1,7] = sd(unlist(total.conserved.3q)/length(total.res.3q))
		fp.3q[j-1,7] = mean(unlist(total.added.3q))
		sd.fp.3q[j-1,7] = sd(unlist(total.added.3q))
		sensitivity.4q[j-1,7] = mean(unlist(total.conserved.4q))/length(total.res.4q)
		sd.sensitivity.4q[j-1,7] = sd(unlist(total.conserved.4q)/length(total.res.4q))
		fp.4q[j-1,7] = mean(unlist(total.added.4q))
		sd.fp.4q[j-1,7] = sd(unlist(total.added.4q))

	}

        ## data for the full set, 100% of reads 5 replicates
	file = superpaste(c(kDataBaseDir,"/",method,"/",method,"_100_5_1_1.Rdata"))
	load(file)
	cat(file,"\n")
	sensitivity[4,7] = length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res))/length(total.res)
	fp[4,7] = length(setdiff(selectdegenes(res,p.thresh,lfc.thresh,method), union.res) )
	
	sensitivity.1q[4,7] = length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.1q)) /length(total.res.1q)
	fp.1q[4,7] = length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
               row.names(HTSeq.dat)[which(signal<=quantiles[2])]), union.res.1q))	

	sensitivity.2q[4,7] = length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.2q)) /length(total.res.2q)
	fp.2q[4,7] = length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
               row.names(HTSeq.dat)[intersect(which(signal>quantiles[2]),which(signal<=quantiles[3]))]), union.res.2q))	

	sensitivity.3q[4,7] = length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.3q)) /length(total.res.3q)
	fp.3q[4,7] = length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
               row.names(HTSeq.dat)[intersect(which(signal<=quantiles[4]),which(signal>quantiles[3]))]), union.res.3q))	

	sensitivity.4q[4,7] = length(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),total.res.4q)) /length(total.res.4q)
	fp.4q[4,7] = length(setdiff(intersect(selectdegenes(res,p.thresh,lfc.thresh,method),
               row.names(HTSeq.dat)[which(signal>quantiles[4])]), union.res.4q))		
		

        ## save method results ##
        save(fp,sd.fp,sensitivity,sd.sensitivity,
             fp.1q,sd.fp.1q,sensitivity.1q,sd.sensitivity.1q,
             fp.2q,sd.fp.2q,sensitivity.2q,sd.sensitivity.2q,
             fp.3q,sd.fp.3q,sensitivity.3q,sd.sensitivity.3q,
             fp.4q,sd.fp.4q,sensitivity.4q,sd.sensitivity.4q,
             file=paste("../data/DepthCoverage_",methods[index.method],"_",Sys.Date(),".Rdata", sep=''))
}


